package instrument

import "time"

type RequestData struct {
	Section  string
	ID       string
	Search   string
	Fmt      string
	Duration time.Duration
}

type Instrument interface {
	Request(req RequestData)
}
